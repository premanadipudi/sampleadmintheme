     <div class="content-footer white " id="content-footer">
         <div class="d-flex p-3">
             <span class="text-sm text-muted flex">&copy; Copyright. Flatfull</span>
             <div class="text-sm text-muted">Version 1.2.0</div>
         </div>
     </div>
            
    <!-- build:js assets/js/app.min.js -->
        <!-- jQuery -->
        <script src="/libs/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- core -->
        <script src="/libs/pace-progress/pace.min.js"></script>
        <script src="/libs/pjax/pjax.min.js"></script>
        <script src="/assets/js/lazyload.config.js"></script>
        <script src="/assets/js/lazyload.js"></script>
        <script src="/assets/js/plugin.js"></script>
        <script src="/assets/js/nav.js"></script>
        <script src="/assets/js/scrollto.js"></script>
        <script src="/assets/js/toggleclass.js"></script>
        <script src="/assets/js/theme.js"></script>
        <script src="/assets/js/ajax.js"></script>
        <script src="/assets/js/app.js"></script>
        <!-- endbuild -->
    
</body>             
                
                