package com.primecore.SampleadminTheme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleadminThemeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleadminThemeApplication.class, args);
	}

}
